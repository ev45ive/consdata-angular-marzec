import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  { path: '', redirectTo: 'playlists', pathMatch: 'full' },
  { 
    path: 'music', 
    loadChildren: 'app/music/music.module#MusicModule'
  },
  { path: '**', redirectTo: 'playlists', pathMatch: 'full' }
]

export const Routing = RouterModule.forRoot(routes, {
  enableTracing: true,
  useHash: true,
  // onSameUrlNavigation: 'ignore',
  // errorHandler: () => {}
})
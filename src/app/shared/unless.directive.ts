import { Directive, TemplateRef, ViewContainerRef, Input, ViewRef, OnDestroy } from '@angular/core';
import { NgForOfContext } from '@angular/common';
NgForOfContext

@Directive({
  selector: '[unless]'
})
export class UnlessDirective {

  // will it change detect?
  cache:ViewRef

  @Input()
  set unless(hide: boolean) {
    if (hide) {
      // this.vcr.clear()
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl, {
          $implicit: 'ala ma kota',
          message: 'wiadomosc od kota'
        }, 0)
      }
    }
  }

  ngOnDestroy(){
    this.cache.destroy()
  }  

  @Input('unlessElse')
  set unlessElse(elses){
    console.log(elses)
  }

  constructor(
    private vcr: ViewContainerRef,
    private tpl: TemplateRef<any>,
  ) {  }


  // ngOnInit(){

  // }

  // ngOnChanges(changes){
  //   console.log(changes)
  // }
}

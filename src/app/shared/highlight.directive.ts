import { Directive, ElementRef, Input, OnInit, HostListener, HostBinding } from '@angular/core';


@Directive({
  selector: '[highlight]',
  // host:{
  //   '(mouseenter)':'onEnter($event.target)'
  //   '[style.color]': 'getColor()'
  // }
})
export class HighlightDirective implements OnInit {
  
  ngOnInit() {
    // console.log(this.color)
    // this.elem.nativeElement.style.color = this.color
  }

  @Input('highlight')
  color
  
  active = false;
  
  @HostBinding('style.color')
  get getColor(){
    return this.active? this.color : 'initial'
  }

  @HostListener('mouseenter',['$event.x'])
  onEnter(x:number){
    // this.elem.nativeElement.style.color = this.color
    // this.active_color = this.color
    this.active = true
  }

  @HostListener('mouseleave')
  onLeave(){
    // this.elem.nativeElement.style.color = 'initial'
    this.active = false
  }

  constructor(private elem:ElementRef) {
    // console.log('hello highlight', this.elem)

  }


}

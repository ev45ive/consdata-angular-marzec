import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { HighlightDirective } from './highlight.directive';
import { UnlessDirective } from './unless.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CardComponent, HighlightDirective, UnlessDirective],
  exports: [CardComponent, HighlightDirective, UnlessDirective]
})
export class SharedModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'card',
  template: `
      <div class="card">
        <div class="card-body">
          <h3 class="card-title">
              <ng-content select="title"></ng-content>
            </h3>
          <ng-content></ng-content>


          <ng-content select=".footer"></ng-content>
        </div>
      </div>
  `,
  styles: []
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

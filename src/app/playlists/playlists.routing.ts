import { Routes, RouterModule } from '@angular/router'
import { PlaylistsComponent } from './playlists.component';
import { PlaylistContainerComponent } from './playlist-container.component';

const routes: Routes = [
  {
    path: 'playlists', component: PlaylistsComponent, children: [
      {
        path: '',
        component: PlaylistContainerComponent
      },
      {
        path: ':id',
        component: PlaylistContainerComponent
      }
    ]
  },
]

export const Routing = RouterModule.forChild(routes)
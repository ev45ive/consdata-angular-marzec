import { Component, OnInit } from '@angular/core';
import { Playlist } from '../models/playlist';
import { PlaylistsService } from './playlists.service';
import { Router } from '@angular/router';

@Component({
  selector: 'playlists',
  template: `
    <div class="row">
      <div class="col">

      <items-list [items]="playlists$ | async"
          [selected]="selected"
          (selectedChange)="select($event)"
        >
        </items-list>

      </div>
      <div class="col">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {
  
  selected:Playlist

  playlists$ = this.service.getPlaylists()
  
  constructor(public service:PlaylistsService,
    private router:Router) { }

  select(playlist:Playlist){
    this.selected = playlist
    this.router.navigate(['playlists', playlist.id])
  }
  
  ngOnInit() {
  }

}

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from '../models/playlist';

enum MODES {
  show,
  edit
}

@Component({
  selector: 'playlist-details',
  template: `
    <div>
      <ng-container [ngSwitch]="mode">
        
          <card *ngSwitchDefault>
              <ng-container ngProjectAs="title">
                Playlist Details
              </ng-container>

              <div class="form-group">
                <label>Name</label>
                <p>{{ playlist.name }}</p>
              </div>
              <div class="form-group">
                <label>Favourite</label>
                <p> {{ playlist.favourite? 'Yes' : 'No' }} </p>
              </div>
              <div class="form-group">
                <label>Color</label>
                <p [style.color]="playlist.color">{{ playlist.color }}</p>
              </div> 
              <button class="btn btn-info"(click)="edit()">Edit</button>       
          </card>

          <card *ngSwitchCase=" 'edit' ">
            <ng-container ngProjectAs="title">
              Edit Playlist 
            </ng-container>
              <form #formRef="ngForm">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" [ngModel]="playlist.name" name="name" required>
                  </div>
                  <div class="check-group">
                    <label> 
                      <input type="checkbox" [ngModel]="playlist.favourite" name="favourite"> Favourite</label>
                  </div>
                  <div class="form-group">
                    <label>Color</label>
                    <input type="color" [(ngModel)]="playlist.color" name="color">
                  </div>
                  <button class="btn btn-danger"(click)="cancel()">Cancel</button>
                  <button class="btn btn-success"(click)="save(formRef)">Save</button>
                </form>
          </card>

        </ng-container>
    </div>
  `,
  styles: [],
  // inputs:[
  //   'playlist:playlist'
  // ]
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  @Output()
  playlistSave = new EventEmitter<Playlist>()

  save(form){
    this.playlistSave.emit({...this.playlist, ...form.value})
    this.mode = 'show'
  }

  mode = 'show'

  edit(){
    this.mode  = 'edit'
  }

  cancel(){
    this.mode  = 'show'
  }


  constructor() { }

  ngOnInit() {
  }

}

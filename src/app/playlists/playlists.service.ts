import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Playlist } from '../models/playlist';
import { delay, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PlaylistsService {

  constructor() { }

  getPlaylist(id) {
    return this.playlists.pipe(
      map(playlists => playlists.find(
        p => p.id == id
      )))
    // .pipe(
    //   delay(2000)
    // )
  }

  getPlaylists() {
    return this.playlists
  }

  updatePlaylist(playlist: Playlist) {
    const found = this.playlists.value.findIndex(
      p => p.id == playlist.id
    )
    let playlists = [
      ...this.playlists.value
    ]
    playlists.splice(found, 1, playlist)

    this.playlists.next(playlists)
  }

  playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 1,
      name: 'Angular Greatest Hits',
      favourite: false,
      color: '#ff0000',
    }, {
      id: 2,
      name: 'The best of Angular',
      favourite: true,
      color: '#00ff00',
    }, {
      id: 3,
      name: 'Angular TOP 20!',
      favourite: false,
      color: '#0000ff',
    }
  ])

}

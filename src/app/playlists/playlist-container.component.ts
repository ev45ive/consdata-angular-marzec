import { Component, OnInit } from '@angular/core';
import { Playlist } from '../models/playlist';
import { PlaylistsService } from './playlists.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import { pluck, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'playlist-container',
  template: `
    <div *ngIf="playlist$ | async as playlist; else noPlaylist">  
      <playlist-details [playlist]="playlist" 
      (playlistSave)="save($event)">
      </playlist-details>    
    </div>

    <ng-template #noPlaylist>
      Please Select Playlist
    </ng-template>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist$ = this.route.params.pipe(
    pluck('id'),
    switchMap((id: string) => this.service.getPlaylist(parseInt(id)))
  )

  save(playlist) {
    this.service.updatePlaylist(playlist)
  }

  constructor(
    public service: PlaylistsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

}

import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';

interface Named{
  name:string
}
interface Item extends Named{
  color: string
}


// (mouseenter)="hover = item"
// (mouseleave)="hover = null"
// [style.color]=" (hover == item?  item.color : 'initial') "

@Component({
  selector: 'items-list',
  template: `
      <div class="list-group">
        <div class="list-group-item"
            (click)="select(item)"
            *ngFor="let item of items"
            [style.borderLeftColor]=" item.color "
            [highlight]="item.color"
            [class.active]=" selected == item ">
          {{item.name}}
        </div>
      </div>
  `,
  encapsulation: ViewEncapsulation.Emulated,
  styles: [`
    .list-group-item{
      border-left: 15px solid;
    }

    :host(.colored) ::ng-deep p{
      color: hotpink;
    }
    :host(.bordered){
      border:1px solid black;
      display:block;
    }
    :host-context(.in-red-zone) p {
      color:red;
    }
  `]
})
export class ItemsListComponent<T extends Item> implements OnInit {

  hover:Item

  @Input()
  selected: T

  @Input('items')
  items: T[]

  @Output()
  selectedChange = new EventEmitter<T>()

  select(item: T) {
    this.selectedChange.emit(item)
  }

  constructor() { }

  ngOnInit() {
  }

}

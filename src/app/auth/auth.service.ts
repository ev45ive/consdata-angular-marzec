import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor() { }

  token: string = null

  authorize() {
    const params = new HttpParams({
      fromObject: {
        client_id: '3e580c81d64e43b98d7da7d96beb56b2',
        response_type: 'token',
        redirect_uri: 'http://localhost:4200/',
      }
    })
    const url = 'https://accounts.spotify.com/authorize?' + params.toString()

    window.localStorage.removeItem('token')
    window.location.replace(url)
  }

  getToken() {
    this.token = JSON.parse(window.localStorage.getItem('token'))

    if(!this.token){
      const match = new HttpParams({
        fromString:window.location.hash.substr(1)
      })
      this.token = match.get('access_token')
      window.location.hash = ''
    }

    if(!this.token){
      this.authorize()
    }
    window.localStorage.setItem('token', JSON.stringify(this.token))
    return this.token
  }
}

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    

    const headers = req
        .headers
        .set('Authorization','Bearer ' + this.auth.getToken())
    
    const authenticatedRequest = req.clone({
      headers
    })

    return next.handle(authenticatedRequest).pipe(
      catchError( (err, caught) => {
       
        if(err instanceof HttpErrorResponse && err.status === 401){
            this.auth.authorize()

            // refreshtoken => caught
        }
        return []
      })
    )
  }
}

// return next.handle(req.clone({
//   headers: req.headers.set('Authorization','Bearer ' + this.auth.getToken())
// }))
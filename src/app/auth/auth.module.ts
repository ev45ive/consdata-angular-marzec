import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth.service';
import { AuthInterceptorService } from './auth-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
  ]
})
export class AuthModule {
  constructor(private auth: AuthService) {
    this.auth.getToken()
  }
  // static forChild(){}

  static forRoot(config?:{
    /**
     * Name of cookie to store token
     */
    cookie_name:string
  }):ModuleWithProviders{
    return {
      ngModule:AuthModule,
      providers:[
        AuthService, 
        AuthInterceptorService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptorService,
          multi: true
        }
      ]
    }
  }
}

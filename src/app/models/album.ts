
export interface Idenity{
  id: string;
}
export interface Named{
  name: string
}

export interface Entity extends Idenity, Named{}

export interface Album extends Entity{
  images: AlbumImage[];
  artists?: Artist[];
  tracks?: Track[]
}

export interface Track extends Entity{}

export interface Artist extends Entity{ }

export interface AlbumImage {
  url: string;
  width: number;
  height: number;
}
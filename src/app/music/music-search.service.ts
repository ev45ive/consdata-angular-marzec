import { Injectable, Inject, InjectionToken } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { Album } from '../models/album';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

export const MUSIC_SEARCH_URL = new InjectionToken<string>('MUSIC_SEARCH_URL')

interface AlbumsResponse {
  albums: {
    items: Album[]
  }
}

// import 'rxjs/Rx'
// import 'rxjs/add/operator/map'
import { map, pluck, startWith, switchMap, filter } from 'rxjs/operators'

@Injectable()
export class MusicSearchService {

  albums$ = new BehaviorSubject<Album[]>([])
  queries$ = new BehaviorSubject<string>('')

  constructor(
    @Inject(MUSIC_SEARCH_URL) private url,
    // @Inject(HttpClient) private http: HttpClient
    private http: HttpClient
  ) {

    this.queries$
      .pipe(
        filter(({ length }) => length > 0),
        map(query => ({
          q: query,
          type: 'album'
        })),
        switchMap(params => this.http.get<AlbumsResponse>(this.url, { params })),
        map(response => response.albums.items)
      )
      .subscribe(albums => {
        this.albums$.next(albums)
      })
  }

  getAlbums() {
    return this.albums$ //.pipe( startWith(this.albums) )
  }

  search(query: string) {
    this.queries$.next(query)
  }

}

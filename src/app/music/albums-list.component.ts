import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../models/album';

@Component({
  selector: 'albums-list',
  template: `
   <!-- .card-group>.card>img.card-img-top[src]+.card-body>h5.card-title>{Card title} -->

    <div class="card-group">
      <album-item class="card" 
                  [album]="album"
                  *ngFor="let album of albums">
      </album-item>
    </div>
  `,
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums: Album[] = [
    {
      id: '123', 
      name: 'Test', 
      images: [
        { 
          url: 'http://via.placeholder.com/300x300/', width: 300, 
          height: 300 
        }
      ]
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}

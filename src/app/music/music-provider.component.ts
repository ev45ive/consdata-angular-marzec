import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from './music-search.service';

@Component({
  selector: 'music-provider',
  template: `
    <ng-content></ng-content>
  `,
  providers:[
    MusicSearchService
  ]
})
export class MusicProviderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl, ControlContainer } from '@angular/forms'
import { AppComponent } from '../app.component';

@Component({
  selector: 'validation-messages',
  template: `
    <ng-container *ngIf="field.touched || field.dirty">
      <div *ngIf="field.hasError('required')">Field is required</div>
      <div *ngIf="field.getError('minlength') as error">
        Value too short {{error.actualLength}} / {{error.requiredLength}} 
      </div>
      <ng-content></ng-content>
  </ng-container>
  `,
  styles: []
})
export class ValidationMessagesComponent implements OnInit {

  @Input()
  field: AbstractControl

  @Input()
  set fieldName(path){
    if(path){
      this.field = this.form.control.get(path)
    }
  }

  constructor(private form:ControlContainer, /* private app:AppComponent */) {}

  ngOnInit() {
  }

}

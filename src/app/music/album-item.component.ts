import { Component, OnInit, Input } from '@angular/core';
import { Album, AlbumImage } from '../models/album';

@Component({
  selector: 'album-item, [album-item]',
  template: `
    <img [src]="image.url" class="card-img-top">
    
    <div class="card-body">
      <h5 class="card-title">{{album.name}}</h5>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input('album')
  set gotAlbum(album) {
    this.album = album,
      this.image = album.images[0] || { url: 'nophoto.png' }
  }
  image: AlbumImage
  album: Album

  constructor() { }

  ngOnInit() {
  }

}

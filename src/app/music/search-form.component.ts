import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { filter, distinctUntilChanged, debounceTime, withLatestFrom, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { FormControl, FormGroup, AbstractControl, FormArray, Validators, FormBuilder, ValidatorFn, ValidationErrors, AsyncValidator, AsyncValidatorFn } from '@angular/forms'
import { PartialObserver, Observer } from 'rxjs/Observer';

@Component({
  selector: 'search-form',
  template: `
    <div class="form-group mb-3" [formGroup]="queryForm">
      <input type="text" class="form-control" placeholder="Search" formControlName="query">
      
      <div *ngIf="queryForm.pending">Please wait...</div>

      <validation-messages fieldName="query">
        <div *ngIf="queryForm.get('query').getError('censor') as error"> 
          Can't use word {{error}}
        </div>
      </validation-messages>
    </div>
  `,
  styles: [`
    .form-group .ng-touched.ng-invalid,
    .form-group .ng-dirty.ng-invalid {
        border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  constructor(private fb: FormBuilder) {

    const censor = (badword: string): ValidatorFn => (control: AbstractControl) => {
      if (control.value == badword) {
        return {
          'censor': badword,
          'extra': true
        } as ValidationErrors
      } else {
        return null
      }
    }

    const asyncCensor = (badword: string): AsyncValidatorFn => (control: AbstractControl) => {
      // return this.http.get('url/validation').map( reponse => response.error || null)
      const validator = censor(badword)

      return Observable.create((observer: Observer<ValidationErrors | null>) => {

        setTimeout(() => {
          observer.next(validator(control))
          observer.complete()
        }, 2000)
      })
    }

    this.queryForm = this.fb.group({
      'query': this.fb.control('', [
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ], [
          asyncCensor('batman')
        ]),
      'type': this.fb.control('album'),
    }, {
        validator: []
      })
    console.log(this.queryForm)

    const status$ = this.queryForm.statusChanges
    const values$ = this.queryForm
      .get('query')
      .valueChanges
      .pipe(
        debounceTime(400),
        filter(({ length }) => length >= 3),
        distinctUntilChanged(),
    )

    const validValue$ = status$.pipe(
      filter(state => state == 'VALID'),
      withLatestFrom(values$, (state, value) => value),
      // map( validAndValue => validAndValue[1])
    )
    this.queryChange = validValue$
  }

  queryForm: FormGroup

  @Input()
  set query(query) {
    const field = this.queryForm.get('query') as FormControl;
    field.setValue(query, {})
  }

  @Output()
  queryChange: Observable<string>

  ngOnInit() {

  }

}

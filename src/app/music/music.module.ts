import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthModule } from '../auth/auth.module';
import { environment } from '../../environments/environment';
import { MUSIC_SEARCH_URL, MusicSearchService } from './music-search.service';
import { HttpClient } from 'selenium-webdriver/http';
import { MusicProviderComponent } from './music-provider.component';
import { MusicProviderDirective } from './music-provider.directive';
import { AuthInterceptorService } from '../auth/auth-interceptor.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidationMessagesComponent } from './validation-messages.component';
import { Routing } from './music.routing';
environment

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    AuthModule,
    ReactiveFormsModule,
    Routing
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent, 
    AlbumsListComponent, 
    AlbumItemComponent, MusicProviderComponent, MusicProviderDirective, ValidationMessagesComponent
  ],
  exports: [MusicSearchComponent, MusicProviderComponent, MusicProviderDirective],
  providers:[
    {
      provide: MUSIC_SEARCH_URL,
      useValue: environment.MUSIC_SEARCH_URL
    },
    // {
    //   provide: MusicSearchService,
    //   useFactory: (url,http) => {
    //     return new MusicSearchService(url,http)
    //   }, 
    //   deps: [MUSIC_SEARCH_URL, HttpClient]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpecialConcreteMusicSearchService
    // }
    MusicSearchService
  ]
})
export class MusicModule { }

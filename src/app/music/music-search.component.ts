import { Component, OnInit } from '@angular/core';
import { Album } from '../models/album';
import { MusicSearchService } from './music-search.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'music-search',
  template: `
    <div class="row">
      <div class="col">
        <search-form 
        [query]="queries$ | async"
        (queryChange)="search($event)"></search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <albums-list [albums]="albums$ | async"></albums-list>
      </div>
    </div>
  `,
  styles: [],
  // viewProviders:[
  //   MusicSearchService
  // ]
})
export class MusicSearchComponent implements OnInit {

  albums$: Observable<Album[]>
  queries$

  constructor(private musicSearch: MusicSearchService) {
    this.queries$ = this.musicSearch.queries$
    this.albums$ = this.musicSearch.getAlbums()
      .pipe(
        // map( albums => albums.slice(0,3)),
        // tap(console.log)
      )
  }

  search(query) {
    this.musicSearch.search(query)
  }
  ngOnInit() {
  }
}
